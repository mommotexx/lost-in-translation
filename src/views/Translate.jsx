import TranslateForm from "../components/Translate/TranslateForm";
import TranslateText from "../components/Translate/TranslateText";
import withAuth from "../hoc/withAuth";
import TranslationProvider from "../context/TranslateContext";

const Translate = () => {
  return (
    // Created a provider, that lets me access the state in every child component. 
    <TranslationProvider>
      <main className="w-full flex flex-col justify-center">
        <section className=" w-[20em] md:w-[40em] self-center mt-6 -mb-8">
          <div className="flex items-center justify-center">
            <TranslateForm />
          </div>
        </section>
        <section className=" bg-white my-10 mx-auto h-[30em] w-5/6 rounded-lg lg:w-2/5 shadow-lg overflow-auto">
          <TranslateText />
        </section>
      </main>
    </TranslationProvider>
  );
};
export default withAuth(Translate);
