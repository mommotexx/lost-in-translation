const NotFound = () => {
  return (
    <div className="my-10 mx-auto h-[20em]">
      <p className="text-center text-2xl font-bold drop-shadow-lg">
        ERROR 404. Page not found!
      </p>
    </div>
  );
};
export default NotFound;
