import ProfileActions from "../components/Profile/ProfileActions";
import ProfileHeader from "../components/Profile/ProfileHeader";
import ProfileTranslationList from "../components/Profile/ProfileTranslationList";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";

const Profile = () => {
  const { user } = useUser();

  return (
    <main>
          <ProfileActions />
      <section className=" bg-white my-10 mx-auto h-[30em] rounded-lg lg:w-2/5 shadow-lg flex flex-col items-center overflow-auto">
        <div>
          <ProfileHeader username={user.username} />
          <ProfileTranslationList list={user.translations} />
        </div>
        <div className="h-full flex self-end items-end mx-5 my-3">
        </div>
      </section>
    </main>
  );
};
export default withAuth(Profile);
