import { createContext, useContext, useState } from "react";
import { browserStorageRead } from "../utils/browserStorage";
// Context API
// Creating a contect with name UserContext. This is for exposing state
const UserContext = createContext();

export const useUser = () => {
  return useContext(UserContext);
};

// Creating a provider with name UserProvider, this manages state
const UserProvider = (props) => {
  const [user, setUser] = useState(browserStorageRead("translation-user"));

  const state = {
    user,
    setUser,
  };

  return (
    <UserContext.Provider value={state}>
      {/* Consumer is props.children */}
      {props.children}
    </UserContext.Provider>
  );
};
export default UserProvider;
