import { createContext, useContext, useState } from "react";

const TranslateContext = createContext();

export const useTranslate = () => {
  return useContext(TranslateContext);
};

const TranslationProvider = (props) => {
  const [translation, setTranslation] = useState("");

  const state = {
    translation,
    setTranslation,
  };

  return (
    <TranslateContext.Provider value={state}>
      {props.children}
    </TranslateContext.Provider>
  );
};

export default TranslationProvider;
