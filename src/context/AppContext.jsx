import UserProvider from "./UserContext";

const AppContext = (props) => {
  return (
    <UserProvider>
      {/* Consumer = props.children */}
      {props.children}
    </UserProvider>
  );
};

export default AppContext;
