// Constants that make it easier to change certain values on the site. 
export const STORAGE_KEY_USER = "translation-user";
export const LOGIN_PAGE_FORM_PLACEHOLDER = "Please write your username";
export const TRANSLATE_PAGE_FORM_PLACEHOLDER =
  "What would you like to translate?";
