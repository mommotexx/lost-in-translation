import { createHeaders } from "./index";
const apiUrl = process.env.REACT_APP_API_URL;

// Adds translation to user, and returns the updated user.
export const addTranslationToUser = async (user, translation) => {
  try {
    const response = await fetch(apiUrl + "/" + user.id, {
      method: "PATCH",
      headers: createHeaders(),
      body: JSON.stringify({
        translations: [...user.translations, translation],
      }),
    });
    if (!response.ok) {
      throw new Error("Request could not be completed");
    }

    const result = await response.json();
    return [null, result];
  } catch (error) {
    return [error.message, null];
  }
};

// Clears the translation of user, and setting to empty array. Returns updated user
export const clearTranslationHistory = async (user) => {
  try {
    const response = await fetch(apiUrl + "/" + user.id, {
      method: "PATCH",
      headers: createHeaders(),
      body: JSON.stringify({
        translations: [],
      }),
    });

    if (!response.ok) {
      throw new Error("Request could not be completed");
    }

    const result = await response.json();
    return [null, result];
  } catch (error) {
    return [error.message, null];
  }
};
