import { createHeaders } from "./index";
const apiUrl = process.env.REACT_APP_API_URL;

// Function for check user. If user exist, returns null for error, and user object as user.
// If user exist, returns [null, user}], if error, returns [error, []]
const checkForUser = async (username) => {
  try {
    const response = await fetch(apiUrl + "?username=" + username);
    if (!response.ok) {
      throw new Error("Request could not be completed");
    }
    const user = await response.json();
    return [null, user];
  } catch (error) {
    return [error.message, []];
  }
};

/* Function for creating a user. Returns null for error, and user object if success
   Fetch needs to stringify JSON, and helper headers are imported from index.js inside the api folder
   Could also use axios, which uses XMLHttpRequest, which is supported in more browsers. 
*/
const createUser = async (username) => {
  try {
    const response = await fetch(apiUrl, {
      method: "POST",
      headers: createHeaders(),
      body: JSON.stringify({
        username,
        translations: [],
      }),
    });
    if (!response.ok) {
      throw new Error("Could not create a user");
    }
    const user = await response.json();
    return [null, user]; // if success, returns null for error, and the user
  } catch (error) {
    return [error.message, []];
  }
};

/* Function that's exported, tries to loin user, and calls for checkForUser. If user
   exists, then returns error null, and user. If it doesn't exist, it runs createUser
*/
export const loginUser = async (username) => {
  const [checkError, user] = await checkForUser(username);

  if (checkError !== null) {
    return [checkError, null];
  }

  if (user.length > 0) {
    return [null, user.pop()];
  }

  return await createUser(username);
};
