import { Navigate } from "react-router-dom";
import { useUser } from "../context/UserContext";

/* 
Higher order component. 
Takes in JSX functions
If user is not null (logged in) it returns component and it's props. 
If no user exists, redirect to main page
*/
const withAuth = (Component) => (props) => {
  const { user } = useUser();

  if (user !== null) {
    return <Component {...props} />;
  } else {
    return <Navigate to="/" />;
  }
};

export default withAuth;
