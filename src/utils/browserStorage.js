export const browserStorageSave = (key, value) => {
  sessionStorage.setItem(key, JSON.stringify(value));
};

export const browserStorageRead = (key) => {
  const storage = sessionStorage.getItem(key);
  if (storage) {
    return JSON.parse(storage);
  }
  return null;
};

export const browserStorageRemove = (key) => {
  sessionStorage.removeItem(key);
};
