const TranslateImages = ({ sentence }) => {
  // Replaces special characters to empty. To not have problems with the images
  const filteredString = sentence.replace(/[^a-z]/gi, "");
  const wordList = [...filteredString];
  const image = wordList.map((letter, key) => {
    return (
      <>
        <img
          src={`images/asl/${letter.toLowerCase()}.png`}
          alt={`asl of ${letter}`}
          width="50px"
          height="50px"
        />
      </>
    );
  });

  return image;
};
export default TranslateImages;
