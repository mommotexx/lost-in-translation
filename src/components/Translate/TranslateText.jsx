import { useTranslate } from "../../context/TranslateContext";
import TranslateImages from "./TranslateImages";

const TranslateText = () => {
  const { translation } = useTranslate();

  return (
    <>
      <h1 className="text-center">Translated text to ASL images</h1>
      <div className="flex flex-wrap">
        <TranslateImages sentence={translation} />
      </div>
    </>
  );
};
export default TranslateText;
