import { useForm } from "react-hook-form";
import { addTranslationToUser } from "../../api/translate";
import {
  STORAGE_KEY_USER,
  TRANSLATE_PAGE_FORM_PLACEHOLDER,
} from "../../const/consts";
import { useTranslate } from "../../context/TranslateContext";
import { useUser } from "../../context/UserContext";
import { browserStorageSave } from "../../utils/browserStorage";
import InputForm from "../InputForm/InputForm";
const TranslateForm = () => {
  const { register, handleSubmit } = useForm();
  const { setTranslation } = useTranslate();
  const { user, setUser } = useUser();

  const onSubmit = async ({ translate }) => {
    setTranslation(translate);
    const [error, updatedUser] = await addTranslationToUser(user, translate);

    // Keep UI and Server state in sync, by setting the user to the new data, and saving the data in browser storage
    if (error === null) {
      browserStorageSave(STORAGE_KEY_USER, updatedUser);
      setUser(updatedUser);
    }
  };

  return (
    <>
      <InputForm
        handleSubmit={handleSubmit}
        register={register}
        registerName={"translate"}
        action={onSubmit}
        config={{}}
        side={false}
        placeholder={TRANSLATE_PAGE_FORM_PLACEHOLDER}
      />
    </>
  );
};
export default TranslateForm;
