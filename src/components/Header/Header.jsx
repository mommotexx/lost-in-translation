import { Link, useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import HeaderEmblem from "./HeaderEmblem";
import logo from "./Logo.png";
import splash from "./Splash.svg";

const Header = () => {
  const { user } = useUser();
  const navigate = useNavigate();
  const navToProfile = () => {
    navigate("/profile");
  };

  return (
    <header className="h-20 bg-yellow w-full border-b-4 border-yellow_dark flex">
      <div className="flex items-center justify-between ml-3 my-0 md:mx-auto md:w-9/12">
        <div className="relative">
          <img className="w-16 z-10 absolute" src={logo} alt="" />
          <img className="w-16 z-0 absoulte" src={splash} alt="" />
        </div>
        <h1 className="text-xl lg:text-3xl select-none">Lost in translation</h1>
        <div className="flex w-1/2 justify-end items-center">
          {/* This only renders the emblem and translation link when user is logged in.  */}
          {user?.username ? (
            <>
              <Link to="/translate" className="mr-10 underline">
                Translate
              </Link>
              <p
                className="bg-yellow_dark px-3 rounded-l-full cursor-pointer"
                onClick={navToProfile}
              >
                {user.username}
              </p>
              <HeaderEmblem action={navToProfile} />
            </>
          ) : null}
        </div>
      </div>
    </header>
  );
};
export default Header;
