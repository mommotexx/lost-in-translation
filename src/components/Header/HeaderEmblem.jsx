import emblem from "./emblem.svg";

const HeaderEmblem = ({ action }) => {
  return (
    <div className="mt-1">
      <img
        src={emblem}
        alt="profile emblem"
        className="mr-10 w-16 md:w-16 cursor-pointer"
        onClick={action}
      />
    </div>
  );
};
export default HeaderEmblem;
