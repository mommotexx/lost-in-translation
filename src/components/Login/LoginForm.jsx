import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { loginUser } from "../../api/user";
import { browserStorageSave } from "../../utils/browserStorage";
import InputForm from "../InputForm/InputForm";
import "animate.css";
import {
  LOGIN_PAGE_FORM_PLACEHOLDER,
  STORAGE_KEY_USER,
} from "../../const/consts";

// Defines restriction for username input. can not contain space or special characters, due to pattern
const usernameConfig = {
  required: true,
  minLength: 4,
  maxLength: 12,
  pattern: /^[a-z0-9]+$/gi,
};

const LoginForm = () => {
  // Hooks
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const { user, setUser } = useUser();
  const navigate = useNavigate();

  // Local state
  const [loggingIn, setLoggingIn] = useState(false);
  const [apiError, setApiError] = useState(null);

  // Checks if user state has a user, if yes, navigate to the other link. Listens to changes on user state and navigate
  useEffect(() => {
    if (user !== null) {
      navigate("/translate");
    }
  }, [user, navigate]);

  // Event handlers
  /* Runs when pressing the submit button. Destructure error and user from loginUser function call.
     If error when fetching, local state apiError will get a value.
     Checks if user exsist after logging in, and storing it in Local Storage for the browser. 
  */
  const onSubmit = async ({ username }) => {
    setLoggingIn(true);
    // Simple way to set username to lowercase, so you can't create users with caps
    const usernameLower = username.toLowerCase();
    const [error, userResponse] = await loginUser(usernameLower);
    if (error !== null) {
      setApiError(error);
    }
    if (userResponse !== null) {
      browserStorageSave(STORAGE_KEY_USER, userResponse);
      setUser(userResponse);
    }
    setLoggingIn(false);
  };

  // Error message function, if error, check what error and return the error message. Runs right after initialization
  const errorMessage = (() => {
    if (errors.username) {
      switch (errors.username.type) {
        case "minLength":
          return `Username must be longer than ${usernameConfig.minLength} characters`;

        case "maxLength":
          return `Username can't contain more than ${usernameConfig.maxLength} characters`;

        case "pattern":
          return `Username can't contain special letters or spaces`;

        case "required":
          return `Username can't be empty`;

        default:
          return `An error occured`;
      }
    }
  })();
  // Returns JSX
  return (
    // Multiple divs to keep the login form in center, and for styling. Check index.css for tailwind documentation link
    <div className="bg-yellow h-96 flex items-center">
      <div className="flex items-center justify-center w-4/5 bg-white my-0 mx-auto h-2/5 rounded-lg lg:w-2/5 shadow-lg border-b-purple border-b-8">
        <InputForm
          handleSubmit={handleSubmit}
          register={register}
          registerName={"username"}
          action={onSubmit}
          config={usernameConfig}
          side={loggingIn} // Side action, here it's if the user is logging in or not
          placeholder={LOGIN_PAGE_FORM_PLACEHOLDER}
        />
        <p className="absolute mt-24 -ml-10 text-red-400">
          {errorMessage} {apiError}
        </p>
        <p className="absolute mt-24 -ml-10 text-blue-400">
          {loggingIn && <span>Logging in...</span>}
        </p>
      </div>
    </div>
  );
};
export default LoginForm;
