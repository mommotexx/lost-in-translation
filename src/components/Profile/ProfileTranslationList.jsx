import ProfileTranslationListItem from "./ProfileTranslationListItem";

const ProfileTranslationList = ({ list }) => {
  let translationListItems;
  /* checks if the length of the array is less than ten,
   if it is, render them normally. If more than ten, render the last 10 translations 
   This was a part of the assignment. 
   "The profile page must display the last 10 translations for the current user"*/
  if (list.length < 10) {
    translationListItems = list.map((translation, index) => (
      <ProfileTranslationListItem item={translation} key={index} />
    ));
  } else {
    translationListItems = list
      .slice(list.length - 10, list.length)
      .map((translation, index) => (
        <ProfileTranslationListItem item={translation} key={index} />
      ));
  }
  return (
    <div>
      <h4 className="text-lg">Your translation history:</h4>
      <ul className="italic">{translationListItems}</ul>
    </div>
  );
};
export default ProfileTranslationList;
