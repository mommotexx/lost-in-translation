import { clearTranslationHistory } from "../../api/translate";
import { STORAGE_KEY_USER } from "../../const/consts";
import { useUser } from "../../context/UserContext";
import {
  browserStorageRemove,
  browserStorageSave,
} from "../../utils/browserStorage";

const ProfileActions = () => {
  const { user, setUser } = useUser();

  // Logout function, removes the browser storage, and set the user state to null. Autologout because hoc withAuth
  const handleLogout = () => {
    if (window.confirm("Do you want to log out?")) {
      browserStorageRemove(STORAGE_KEY_USER);
      setUser(null);
    }
  };

  // Runs when pressing the clear translation button. Passing in the user, and returning empty array for translations
  const handleTranslationClear = async () => {
    if (window.confirm("Are you sure you want to delete your history?")) {
      const [error, updatedUser] = await clearTranslationHistory(user);
      if (error === null) {
        browserStorageSave(STORAGE_KEY_USER, updatedUser);
        setUser(updatedUser);
      }
    }
  };

  return (
    <div className="flex justify-end gap-4 mr-4 2xl:mr-[20em] mt-4">
      <button
        className=" bg-red-500 px-2 rounded"
        onClick={handleTranslationClear}
      >
        Clear list
      </button>
      <button className="bg-blue-200 px-2 rounded" onClick={handleLogout}>
        Log out
      </button>
    </div>
  );
};
export default ProfileActions;
