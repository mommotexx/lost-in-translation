const ProfileHeader = ({ username }) => {
  return (
    <>
      <h3 className="mt-2 mb-2 text-2xl border-b-[1px] w-full text-center">
        Hello, welcome back <span className="font-bold">{username}</span>
      </h3>
    </>
  );
};
export default ProfileHeader;
