import { ImKeyboard } from "react-icons/im";
import { BsFillArrowRightCircleFill } from "react-icons/bs";

/* Extra component I didn't consider when creating component-tree.
 Found out after that LoginForm was too long, 
 and I need the input in translate part as well */
const InputForm = ({
  handleSubmit,
  register,
  registerName,
  action,
  config,
  side,
  placeholder,
}) => {
  return (
    <form
      onSubmit={handleSubmit(action)}
      className="p-2 border-2 rounded-full md:w-3/4 bg-white"
    >
      <div className="flex">
        {/* Imkeyboard imported from react-icons. Just SVG icons in JSX element */}
        <ImKeyboard className="text-4xl mr-4 pr-2 border-r-2 border-gray" />
        <input
          {...register(registerName, config)}
          type="text"
          placeholder={placeholder}
          className="text-black w-full outline-none"
          autoComplete="off"
        />
        {/* Button with className animate from https://animate.style/  */}
        <button
          type="submit"
          className="ml-auto animate__animated animate__rubberBand"
          disabled={side}
        >
          {/* Arrow icon imported from react-icons, with className for styling, ternary expression changes button color when logging in */}
          <BsFillArrowRightCircleFill
            className={"text-4xl ml-4 " + (side ? "text-gray" : "text-purple")}
          />
        </button>
      </div>
      {/* Paragraph with absolute position, with content of errorMessage (if any errors) */}
    </form>
  );
};
export default InputForm;
