/** @type {import('tailwindcss').Config} */
const colors = require("tailwindcss/colors");

module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors: {
      yellow: "#FFC75F",
      yellow_dark: "#E7B355",
      purple: "#845EC2",
      light_gray: "#EFEFEF",
      gray: "#969696",
      red: colors.red,
      blue: colors.blue,
      white: colors.white,
    },
    extend: {},
  },
  plugins: [],
};
